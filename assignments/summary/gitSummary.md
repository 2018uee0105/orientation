**GIT SUMMARY**

* Git is a distributed version-control software. This essentially means that it keeps track of all the changes that a person or their team makes in software development, making it easier to collaborate and return to a previous stage if requirement occurs.

* Git is _distributed_ VCS which means that unlike central VCS,  every contributor has a local clone of the main repository on their own machines. This is very helpful as it removes the requirement of internet while committing changes(also making basic operations faster) and providing security in case the remote server is damaged. Also while working with a team, the code could be shared between collaborators to review before pushing it t the main repository in the remote server.

**BASIC GIT KEYWORDS**
1. **Repository** (also known as ‘**Repo**’):  
 A repository is the collection of files and folders being tracked by git. This collection of files and folders contains all the code that the team members work on

2.  **GitLab**:  
Gitlab is a service that provides remote access to git repositories. It is the 2nd most popular remote storage solution after Github.

3.	**Commit**:  
Committing saves a copy of the work, as it is in that particular moment, on one’s local machine. This copy is copy is accessible to the person in case they want to revert to that position. This copy exists on the local machine only until it is pushed to a remote repository.  
Code : 
> $ git commit -sv   
4.	**Push**:  
Pushing saves the committed work to the remote repository.   
Code : 
> $ git push origin <branch-name> 
5.	**Branch**:  
Branches are separate instances of the code that is different from the main codebase. The work originates from the analogy that the git repo is a tree, the main software is the trunk or the master branch and further it’s branches are called branches.  
Code to create a new branch: 
> $ git checkout master  
$ git checkout -b <your-branch-name>

6.	**Merge**:  
Merge refers to merging of two branches. When one is satisfied that the changes they have made in their branch are correct and error-free, and want to add those features to the The primary codebase, they merge it to the master branch.  

7.	**Clone**:  
Cloning takes the complete online repository, makes an exact copy of it and stores it on one’s local machine.  
Code : 
> $ git clone <link-to-repository> 
8.	**Fork**:  
Forking creates a new repo similar to an existing repo under one’s own name. 

**GIT INTERNALS**

Git has three main file states :
* Modified : File has been changed but has not been committed yet.
* Staged : The modified file has been marked to go into the next snapshot( to get committed).
* Committed : Data has been safely stored in the local repo as snapshots.

The following images explain these stages and their respective trees better.  
![internals](images/internals.png)


**GIT WORKFLOW** 
* Clone the repo.  
* Create a new branch and select it.  
* Modify the files and only stage those changes which need to be committed.  
* Commit the changes to the Local Repo.  
* Push the changes to the remote repo.  

