**GITLAB**


GitLab is an open source code repository and collaborative development platform.

In simple words, it offers a location for online code storage and shared development of massive software projects. Based on git, it has version control, which allows users to inspect previous code and revert to it in case of any issues. GitLab is  2nd most popular in git services after Github.

**Highlighted Features:**

* Open source code library
* Free hosting and services
* Bug tracking mechanism
* File editing in the web interface
* LDAP integration (Lightweight Directory Access Protocol)

**GitLab Interface** :

![interface](images/interface.png)


